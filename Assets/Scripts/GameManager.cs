﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;
    public GameObject menu;
    public GameObject gameUI;
    public GameObject game;
    public GameObject settings;
    public GameObject loseScreen;
    public GameObject pause;
    void Start()
    {
        GM = this;
        menu.gameObject.SetActive(true);
        gameUI.gameObject.SetActive(false);
        game.gameObject.SetActive(false);
        settings.gameObject.SetActive(false);
        loseScreen.gameObject.SetActive(false);
        pause.gameObject.SetActive(false);
    }
    void Update()
    {
        
    }
}
