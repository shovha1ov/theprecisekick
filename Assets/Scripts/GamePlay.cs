﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : MonoBehaviour
{
    public static GamePlay GP;

    public int score;
    public int checkGoal;
    [Space(20)]
    public int direction;
    public float speedPos;
    public float speedPosBall;
    public GameObject exhaust;
    public GameObject ball;
    public Vector2 startPosBall;
    public Vector2 endPosBall;
    public Vector2 ballNextPos;
    public Vector2 ballScale;
    public Vector2 ballStartScale;
    public GameObject horizontal;
    public GameObject horizontalChild;
    public GameObject vertical;
    public GameObject verticalChild;
    public Vector2 xPos;
    public Vector2 yPos;
    public Vector2 nextPosX;
    public Vector2 nextPosY;
    public Transform targetMaxX;
    public Transform targetMinX;
    public Transform targetMaxY;
    public Transform targetMinY;

    public AudioSource myFx;
    public AudioClip ballFx;
    public bool isFx;

    public bool checkMousePos;
    void Start()
    {
        GP = this;
        nextPosX = targetMaxX.position;
        nextPosY = targetMaxY.position;
        horizontal.gameObject.SetActive(true);
        vertical.gameObject.SetActive(false);
        startPosBall = ball.transform.position;
        ballStartScale = ball.transform.localScale;
        ball.GetComponent<CircleCollider2D>().enabled = false;
    }

    void Update()
    {
        if (direction != 3)
            MoveControl();
        if (direction != 2)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if(touchMouse())
                    direction += 1;
            }
        }
    }
    public void MoveControl()
    {
        if (direction == 0)
        {
            xPos = Vector2.MoveTowards(horizontalChild.transform.position, nextPosX, speedPos * Time.deltaTime);
            horizontalChild.transform.position = xPos;
            if (horizontalChild.transform.position == targetMaxX.position)
            {
                nextPosX = targetMinX.position;
            }
            if (horizontalChild.transform.position == targetMinX.position)
            {
                nextPosX = targetMaxX.position;
            }
        }
        else if (direction == 1)
        {
            vertical.gameObject.SetActive(true);
            yPos = Vector2.MoveTowards(verticalChild.transform.position, nextPosY, speedPos * Time.deltaTime);
            verticalChild.transform.position = yPos;
            if (verticalChild.transform.position == targetMinY.position)
            {
                nextPosY = targetMaxY.position;
            }
            if (verticalChild.transform.position == targetMaxY.position)
            {
                nextPosY = targetMinY.position;
            }
        }
        else if (direction == 2)
        {
            horizontal.gameObject.SetActive(false);
            vertical.gameObject.SetActive(false);
            if (isFx == true)
            {
                ballSound();
                isFx = false;
            }
            endPosBall = new Vector2(horizontalChild.transform.position.x, verticalChild.transform.position.y);
            ballNextPos = Vector2.MoveTowards(ball.transform.position, endPosBall, speedPosBall * Time.deltaTime);
            ball.transform.position = ballNextPos;
            ballScale = Vector3.MoveTowards(ball.transform.localScale, new Vector3(0.35f, 0.35f, 0.35f), 5f * Time.deltaTime);
            ball.transform.localScale = ballScale;
            if (ball.transform.position == new Vector3(endPosBall.x, endPosBall.y, 0))
            {
                ball.GetComponent<CircleCollider2D>().enabled = true;
                Invoke("CheckGoal", 0.05f);
                direction = 3;
                isFx = true;
            }
        }
    }
    public void CheckGoal()
    {
        if (checkGoal == 0)
        {
            if (score > UI.userInterface.bestScoreUI)
            {
                PlayerPrefs.SetInt("BestScore", score);
                UI.userInterface.bestScoreUI = PlayerPrefs.GetInt("BestScore");
            }
            GameObject tempExhaust = Instantiate(exhaust, new Vector3(endPosBall.x, endPosBall.y, -0.1f), Quaternion.identity, ObstacleGenerate.OG.parentObstacle);
            Destroy(tempExhaust.gameObject, 1f);
            GameManager.GM.gameUI.gameObject.SetActive(false);
            GameManager.GM.loseScreen.gameObject.SetActive(true);
            checkGoal = 1;
            speedPos = 3;
            score = 0;
        }
        else
        {
            score += 1;
            UI.userInterface.score = score;
            if (score % 11 == 0 && ObstacleGenerate.OG.count != 4)
            {
                ObstacleGenerate.OG.count += 1;
            }
            if (score % 2 == 0)
            {
                speedPos += 0.25f;
            }
            direction = 0;
            nextPosX = targetMaxX.position;
            nextPosY = targetMaxY.position;
            horizontal.gameObject.SetActive(true);
            vertical.gameObject.SetActive(false);
            ball.GetComponent<CircleCollider2D>().enabled = false;
            ball.transform.position = startPosBall;
            ball.transform.localScale = ballStartScale;
            ObstacleGenerate.OG.SpawnObstacle();
            Destroy(ObstacleGenerate.OG.parentObstacle.GetChild(0).gameObject);
            Debug.Log(score);
        }
    }
    public void RestartGame()
    {
        direction = 0;
        UI.userInterface.score = score;
        ObstacleGenerate.OG.count = 0;
        nextPosX = targetMaxX.position;
        nextPosY = targetMaxY.position;
        horizontal.gameObject.SetActive(true);
        vertical.gameObject.SetActive(false);
        ball.GetComponent<CircleCollider2D>().enabled = false;
        ball.transform.position = startPosBall;
        ball.transform.localScale = ballStartScale;
        ObstacleGenerate.OG.SpawnObstacle();
        Destroy(ObstacleGenerate.OG.parentObstacle.GetChild(0).gameObject);
    }
    public void ballSound()
    {
        myFx.PlayOneShot(ballFx);
    }
    public bool touchMouse()
    { 
        bool isBtn;
        float height = Screen.height;
        float tempHeight = height * 0.85f;
        if (Input.mousePosition.y > tempHeight) 
        {
            isBtn = false;
        }
        else
        {
            isBtn = true;
        }
        return isBtn;
    }
}
