﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public static UI userInterface;
    [Space(20)]
    public Text bestScoreText;
    public Text bestScore;
    public Text scoreText;
    public Text playText;
    public Text settingsTextBtn;
    public Text settingsText;
    public Text soundText;
    public Text loseText;
    public Text loseScoreText;
    public Text loseRestartText;
    public Text loseMenuText;
    public Text pauseText;
    public Text pauseReturnText;
    public Text pauseRestartText;
    public Text pauseMenuText;
    [Space(20)]
    public AudioListener myAudioListener;
    public int isAudioListener;
    [Space(20)]
    public GameObject soundBtn;
    public GameObject btnUSA;
    public GameObject btnGER;
    public GameObject btnFRA;
    public Sprite[] sprites;
    [Space(20)]
    public float score;
    public float bestScoreUI;


    void Start()
    {
        userInterface = this;
        isAudioListener = PlayerPrefs.GetInt("isListener");
        bestScoreUI = PlayerPrefs.GetInt("BestScore");
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("isAudio") == 0)
        {
            myAudioListener.enabled = false;
            soundBtn.GetComponent<Image>().sprite = sprites[0];
        }
        else if (PlayerPrefs.GetInt("isAudio") == 1)
        {
            myAudioListener.enabled = true;
            soundBtn.GetComponent<Image>().sprite = sprites[1];
        }
        if (PlayerPrefs.GetInt("iFlags") == 0)
        {
            btnUSA.GetComponent<Image>().sprite = sprites[1];
            btnGER.GetComponent<Image>().sprite = sprites[0];
            btnFRA.GetComponent<Image>().sprite = sprites[0];
            bestScoreText.text = "BEST SCORE";
            bestScore.text = "" + bestScoreUI;
            scoreText.text = "SCORE " + score;
            playText.text = "PLAY";
            settingsTextBtn.text = "SETTINGS";
            settingsText.text = "SETTINGS";
            soundText.text = "SOUND";
            loseText.text = "LOSE";
            loseScoreText.text = "SCORE " + score;
            loseRestartText.text = "RESTART";
            loseMenuText.text = "MENU";
            pauseText.text = "PAUSE";
            pauseReturnText.text = "RETURN";
            pauseRestartText.text = "RESTART";
            pauseMenuText.text = "MENU";
        }
        else if (PlayerPrefs.GetInt("iFlags") == 1)
        {
            btnUSA.GetComponent<Image>().sprite = sprites[0];
            btnGER.GetComponent<Image>().sprite = sprites[1];
            btnFRA.GetComponent<Image>().sprite = sprites[0];
            bestScoreText.text = "BESTES ERGEBNIS";
            bestScore.text = "" + bestScoreUI;
            scoreText.text = "ERGEBNIS " + score;
            playText.text = "SPIEGEL";
            settingsTextBtn.text = "EINSTELLEN";
            settingsText.text = "EINSTELLEN";
            soundText.text = "TON";
            loseText.text = "VERLUST";
            loseScoreText.text = "ANZEIGE " + score;
            loseRestartText.text = "RESTART";
            loseMenuText.text = "MENÜ";
            pauseText.text = "PAUSE";
            pauseReturnText.text = "RÜCKKEHR";
            pauseRestartText.text = "RESTART";
            pauseMenuText.text = "MENÜ";
        }
        else
        {
            btnUSA.GetComponent<Image>().sprite = sprites[0];
            btnGER.GetComponent<Image>().sprite = sprites[0];
            btnFRA.GetComponent<Image>().sprite = sprites[1];
            bestScoreText.text = "MEILLEUR SCORE";
            bestScore.text = "" + bestScoreUI;
            scoreText.text = "SCORE " + score;
            playText.text = "JEU";
            settingsTextBtn.text = "RÉGLAGES";
            settingsText.text = "RÉGLAGES";
            soundText.text = "SON";
            loseText.text = "LOSE";
            loseScoreText.text = "SCORE " + score;
            loseRestartText.text = "REDÉMARRER";
            loseMenuText.text = "MENU";
            pauseText.text = "PAUSE";
            pauseReturnText.text = "RETURN";
            pauseRestartText.text = "REDÉMARRER";
            pauseMenuText.text = "MENU";
        }
    }
}
