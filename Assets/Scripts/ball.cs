﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "stena")
        {
            GamePlay.GP.ballSound();
            GamePlay.GP.checkGoal = 0;
        }
    }
}
