﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public AudioSource myFx;
    public AudioClip clickFx;
    public void OnPlay()
    {
        GameManager.GM.menu.gameObject.SetActive(false);
        GameManager.GM.gameUI.gameObject.SetActive(true);
        GameManager.GM.game.gameObject.SetActive(true);
    }
    public void OnSettings()
    {
        GameManager.GM.menu.gameObject.SetActive(false);
        GameManager.GM.settings.gameObject.SetActive(true);
    }
    public void OnPause()
    {
        GameManager.GM.game.GetComponent<GamePlay>().enabled = false;
        GameManager.GM.gameUI.gameObject.SetActive(false);
        GameManager.GM.pause.gameObject.SetActive(true);
    }
    public void OnBack()
    {
        GameManager.GM.menu.gameObject.SetActive(true);
        GameManager.GM.settings.gameObject.SetActive(false);
    }
    public void OnSound()
    {
        if (UI.userInterface.isAudioListener == 0)
        {
            UI.userInterface.isAudioListener = 1;
            PlayerPrefs.SetInt("isListener", UI.userInterface.isAudioListener);
            PlayerPrefs.SetInt("isAudio", 0);
        }
        else
        {
            UI.userInterface.isAudioListener = 0;
            PlayerPrefs.SetInt("isListener", UI.userInterface.isAudioListener);
            PlayerPrefs.SetInt("isAudio", 1);
        }
    }
    public void OnRestart()
    {
        GameManager.GM.game.GetComponent<GamePlay>().enabled = true;
        GameManager.GM.gameUI.gameObject.SetActive(true);
        GameManager.GM.loseScreen.gameObject.SetActive(false); 
        GameManager.GM.pause.gameObject.SetActive(false);
        ObstacleGenerate.OG.count = 0;
        GamePlay.GP.score = 0;
        GamePlay.GP.RestartGame();
    }
    public void OnMenu()
    {
        StartCoroutine(enumerator(0.1f));
    }
    public void OnReturn()
    {
        GameManager.GM.game.GetComponent<GamePlay>().enabled = true;
        GameManager.GM.gameUI.gameObject.SetActive(true);
        GameManager.GM.pause.gameObject.SetActive(false);
    }
    public void OnUSA()
    {
        PlayerPrefs.SetInt("iFlags", 0);
    }
    public void OnGER()
    {
        PlayerPrefs.SetInt("iFlags", 1);
    }
    public void OnFRA()
    {
        PlayerPrefs.SetInt("iFlags", 2);
    }
    public void clickSound()
    {
        myFx.PlayOneShot(clickFx);
    }
    IEnumerator enumerator(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(0);
    }
}
