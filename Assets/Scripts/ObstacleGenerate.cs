﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerate : MonoBehaviour
{
    public static ObstacleGenerate OG;
    public int count;
    public Transform parentObstacle;
    public Transform tempParentObstacle;
    public List<GameObject> obstacles = new List<GameObject>();
    public List<GameObject> targets = new List<GameObject>();
    void Start()
    {
        OG = this;
        SpawnObstacle();
    }
    public void SpawnObstacle()
    {
        Transform tempParent = Instantiate(tempParentObstacle, parentObstacle);
        for(int i = 0; i <= count; i++)
        {
            Instantiate(obstacles[Random.Range(0, 2)], targets[Random.Range(0, targets.Count)].transform.position, Quaternion.identity, tempParent);
        }
    }
}
